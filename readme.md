# Teste Programador Frontend

## Cenário

O Ateliê acaba de receber uma nova conta, e o cliente solicitou uma landing page para uma promoção, onde seja possível efetuar um cadastro.

## Teste:

Criar o HTML/CSS responsivo com base no layout.

O formulário de cadastro deverá ser integrado com a API fornecida para envio dos dados do usuário.

Após submeter o formulário, deverá ser exibido um modal com mensagem de sucesso.

O teste deverá ser entregue através do BitBucket ou GitHub, junto com o teste deverá ser enviado as instruções no README para instalação e execução

## Requisitos

- React ou Angular
- Suporte de dispositivos: Desktop, Smartphone e Tablet.
- Suporte de Browsers: IE9+, Chrome, Safari, Firefox.


## Estrutura dos formulários

### Campos do cadastro

- Nome
- CPF
- E-mail
- Senha
- Confirme sua senha
- Empresa
- Classificação
	- Gerente
	- Revendedor
	- Distribuidor


## API

### Documentação

https://documenter.getpostman.com/view/9196190/2sA3kPqQhe
